import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

class TodoList extends React.Component {

    constructor(props) {
        super(props)

    }

    editTodo = (data) => {
        this.props.editTodo(data)
    }

    deleteTodo = (id) => {
        this.props.deleteTodo(id)
    }

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', margin: 10, borderColor: 'grey', borderWidth: 1, padding: 10, backgroundColor: 'lightgray' }}>
                    <Text style={{ textAlign: 'center', alignSelf: 'center' }}>{this.props.todo}</Text>

                    <View style={{ flex: 1, alignItems: 'flex-end', padding: 5 }}>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {
                                this.editTodo(this.props)
                            }}>
                            <Image source={require('../assets/images/edit.png')} style={{ width: 24, height: 24 }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ alignItems: 'flex-end', padding: 5 }}>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {
                                this.deleteTodo(this.props.id)
                            }}>
                            <Image source={require('../assets/images/delete.png')} style={{ width: 15, height: 24 }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default TodoList;