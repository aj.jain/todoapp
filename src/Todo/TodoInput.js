import React from 'react';
import { View, TextInput, TouchableOpacity, Text, Keyboard } from 'react-native';

class TodoInput extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            todo: '',
            id: '',
            isEdit: false
        }
    }

    componentDidMount() {
        if (this.props.isEdit) {
            this.setState({ todo: this.props.edit.todo, id: this.props.edit.id, isEdit: this.props.isEdit })
        } else {
            this.setState({ isEdit: this.props.isEdit })
        }
    }

    UNSAFE_componentWillReceiveProps = (props) => {
        if (props.isEdit) {
            this.setState({ todo: props.edit.todo, id: props.edit.id, isEdit: props.isEdit })
        } else {
            this.setState({ isEdit: props.isEdit })
        }
    }

    onValueChange = (value, type) => {
        this.setState({ [type]: value });
    }

    addTodo = () => {
        this.setState({ todo: '' })
        Keyboard.dismiss();
        if (this.state.todo.length > 0) this.props.addTodo(this.state.todo)
    }

    updateTodo = () => {
        this.setState({ todo: '' })
        Keyboard.dismiss();
        if (this.state.todo.length > 0) this.props.updateTodo(this.state.todo, this.state.id)
    }

    render() {
        return (
            <View style={{ flexDirection: 'row' }}>
                <TextInput
                    style={{ width: '60%', height: 40, borderColor: 'grey', borderWidth: 1, margin: 15, paddingLeft: 10 }}
                    autoFocus={true}
                    autoCapitalize="none"
                    autoCorrect={false}
                    selectionColor="silver"
                    keyboardType="email-address"
                    returnKeyType="next"
                    placeholder="TODO"
                    placeholderTextColor="silver"
                    value={this.state.todo}
                    onChangeText={text => this.onValueChange(text, 'todo')} />

                <TouchableOpacity
                    activeOpacity={0.6}
                    style={{
                        backgroundColor: '#f57325',
                        paddingVertical: 10,
                        padding: 15,
                        borderRadius: 5,
                        margin: 15,
                        fontWeight: '700',
                    }}
                    onPress={() => this.state.isEdit ? this.updateTodo() : this.addTodo()}
                >
                    <Text style={{
                        color: '#fff',
                        textAlign: 'center',
                        fontWeight: '700',
                    }}>{this.state.isEdit ? 'Update Todo' : 'Add Todo'}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default TodoInput;