import React from 'react';
import { View, ScrollView } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

import TodoList from '../Todo/TodoList';
import TodoInput from '../Todo/TodoInput';

class Dashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            todo: '',
            todoList: [],
            isEdit: false,
            edit: null
        }
        this.todoAdd = []
    }

    static navigationOptions = navigation => ({
        title: 'Dashboard',
        headerStyle: {
            backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    })

    async componentDidMount() {
        let newArray = await AsyncStorage.getItem('list');
        if (newArray != null) {
            this.todoAdd = JSON.parse(newArray);
            this.setState({ todoList: JSON.parse(newArray) })
        }
    }

    onValueChange = (value, type) => {
        this.setState({ [type]: value })
    }

    addTodo = async (todo) => {
        let obj = {};
        obj.todo = todo;

        this.todoAdd.push(obj);

        await AsyncStorage.setItem("list", JSON.stringify(this.todoAdd))

        this.setState({ todoList: this.todoAdd, isEdit: false })
    }

    editTodo = (props) => {
        this.setState({ isEdit: true, edit: props })
    }

    updateTodo = async (todo, id) => {
        let newArray = this.state.todoList.map((item, i) => {
            if (i == id) {
                item.todo = todo;
                return item;
            }

            return item;
        })

        await AsyncStorage.setItem("list", JSON.stringify(newArray))

        this.setState({ todoList: newArray, isEdit: false })
    }

    deleteTodo = async (id) => {
        let newArray = this.state.todoList.filter((item, i) => {
            return i != id;
        })

        this.todoAdd = newArray

        await AsyncStorage.setItem("list", JSON.stringify(newArray))
        this.setState({ todoList: newArray })
    }

    render() {
        return (
            <View>
                <TodoInput
                    addTodo={this.addTodo}
                    updateTodo={this.updateTodo}
                    isEdit={this.state.isEdit}
                    edit={this.state.edit}
                />
                <ScrollView >
                    {this.state.todoList.map((item, i) => {
                        return <TodoList
                            key={i}
                            id={i}
                            todo={item.todo}
                            editTodo={this.editTodo}
                            deleteTodo={this.deleteTodo} />
                    })}
                </ScrollView>
            </View>
        )
    }
}

export default Dashboard;