import React from 'react';
import { View, Text, TextInput, TouchableOpacity, Image } from 'react-native'

import { auth_styles } from '../Auth/AuthStyle'
import { authLogin } from '../Config/api.config'

import AsyncStorage from '@react-native-community/async-storage';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            password: '',
            hidePassword: true,
        }
    }

    onValueChange = (value, type) => {
        this.setState({ [type]: value })
    }

    managePasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    };

    toLogin = async () => {
        if (authLogin(this.state.userName, this.state.password)) {
            await AsyncStorage.setItem('token', "true");
            this.props.navigation.navigate('Main');
        } else {
            alert('Invalid Credentials')
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%' }}>
                <Text style={{ margin: 15, textAlign: 'center', color: '#f57325', fontSize: 20 }}>Login</Text>

                <View style={auth_styles.loginContainer}>
                    <TextInput
                        style={auth_styles.inputText}
                        autoFocus={true}
                        autoCapitalize="none"
                        onSubmitEditing={() => this.passwordInput.focus()}
                        autoCorrect={false}
                        selectionColor="silver"
                        keyboardType="email-address"
                        returnKeyType="next"
                        placeholder="Username"
                        placeholderTextColor="silver"
                        value={this.state.userName}
                        onChangeText={text => this.onValueChange(text, 'userName')}
                    />
                    <View style={auth_styles.passwordInputText}>
                        <TextInput
                            autoCapitalize="none"
                            returnKeyType="go"
                            ref={input => (this.passwordInput = input)}
                            placeholder="Password"
                            selectionColor="silver"
                            placeholderTextColor="silver"
                            secureTextEntry={this.state.hidePassword}
                            value={this.state.password}
                            onChangeText={text => this.onValueChange(text, 'password')}
                        />
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={auth_styles.visibilityBtn}
                            onPress={this.managePasswordVisibility}
                        >
                            <Image
                                source={
                                    this.state.hidePassword
                                        ? require('../assets/images/hide.png')
                                        : require('../assets/images/view.png')
                                }
                                style={auth_styles.btnImage}
                            />
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.6}
                        style={auth_styles.buttonContainer}
                        onPress={() => this.toLogin()}
                    >
                        <Text style={auth_styles.buttonText}>{'Sign In'}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}

export default Login;