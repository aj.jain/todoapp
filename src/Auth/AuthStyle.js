import { StyleSheet } from 'react-native';

const auth_styles = StyleSheet.create({

    loginContainer: {
        height: '70%',
        width: '100%',
        padding: '2%',
        justifyContent: 'center',
        position: 'relative',
    },
    inputText: {
        borderWidth: .4,
        height: 50,
        backgroundColor: '#fff',
        marginBottom: 10,
        padding: 10,
        borderRadius: 4,
        margin: 15,
    },

    passwordInputText: {
        borderWidth: .4,
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#fff',
        marginBottom: 7,
        padding: 5,
        borderRadius: 4,
        paddingRight: 55,
        margin: 15,
    },
    visibilityBtn: {
        position: 'absolute',
        right: 10,
        height: 30,
        width: 30,
        padding: 5,
    },

    btnImage: {
        justifyContent: 'center',
        resizeMode: 'contain',
        height: '100%',
        width: '100%',
        tintColor: 'gray',
    },

    buttonContainer: {
        backgroundColor: '#f57325',
        paddingVertical: 15,
        marginTop: 25,
        margin: 15,
        borderRadius: 4,
        fontWeight: '700',
    },

    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
    },
})

export { auth_styles };