import React from 'react';
import {
    createAppContainer,
    createSwitchNavigator,
} from 'react-navigation'
import {
    createStackNavigator
} from 'react-navigation-stack';

import Login from '../Auth/Login'
import Dashboard from '../Home/Dashboard'
import AuthLoading from '../Components/AuthLoading';

const loginStack = createStackNavigator(
    {
        Login: { screen: Login }
    }, {
        headerMode: 'none',
    }
)

const appStack = createStackNavigator({
    Dashboard: {
        screen: Dashboard,
    }
})


export default createAppContainer(
    createSwitchNavigator(
        {
            AuthLoading: AuthLoading,
            Login: loginStack,
            Main: appStack
        },
        {
            initialRouteName: 'AuthLoading',
        }
    )
)