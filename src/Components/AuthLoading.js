import React from 'react';
import { View, Image, StyleSheet } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

class AuthLoading extends React.Component {

    constructor(props) {
        super(props)

        this.token = false;
    }

    async componentDidMount() {
        this.token = await AsyncStorage.getItem('token');
        setTimeout(() => this.authenticate(), 1000)
    }

    authenticate = () => {
        if (this.token == "true") {
            this.props.navigation.navigate('Main');
        } else {
            this.props.navigation.navigate('Login');
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../assets/images/kk.png')} resizeMode="contain" />
            </View>
        );
    }
}

export default AuthLoading;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
});