/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import AppNavigator from './src/Navigation/AppNavigator';

class App extends React.Component {

  render() {
    return (
      <AppNavigator />
    )
  }

}

export default App;
